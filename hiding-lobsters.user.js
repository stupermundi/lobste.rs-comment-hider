// ==UserScript==
// @name Lobste.rs Comment Tree Hiding
// @author tesla
// @version 0.5.4
// @description Adds a button to hide child comments on lobste.rs
// @icon https://lobste.rs/favicon.ico
// @include https://lobste.rs/s/*
// @include https://lobste.rs/threads
// @match https://lobste.rs/s/*
// @match https://lobste.rs/threads
// @downloadURL https://bitbucket.org/stupermundi/lobste.rs-comment-hider/downloads/hiding-lobsters.user.js
// @updateURL https://bitbucket.org/stupermundi/lobste.rs-comment-hider/downloads/hiding-lobsters.user.js
// @grant none
// ==/UserScript==

var exp = ' | expand';
var col = ' | collapse';

/**
 * Creates a hide/show toggle for the list of elements
 *
 * @param {HTMLOListElement}[] to_toggle a list of fields to hide
 * @return {function(e:Event)} Function that toggles hiding
 */
var collapse_expand = function (to_toggle) {
	return function (e) {
		e.preventDefault();
		this.innerText = this.innerHTML == exp ? col : exp;
		for (var i in to_toggle) {
			to_toggle[i].hidden = !to_toggle[i].hidden;
		}
	};
};

/**
 * forEach style function to add 'collapser' element
 */
var add_collapser = function (cur, _ndx, _arr) {
	var main_comment = cur.children[0];
	var child_comments = cur.children[1];

	var to_collapse = []

	// Create anchor element
	var collapser = document.createElement("a");
	collapser.innerText = col;

	var byline;
	// Find byline element for main comment
	// If deleted, details is the only element, otherwise it's the second
	if (main_comment.childElementCount == 1) {
		byline = main_comment.children[0].children[0];
	} else {
		var details = main_comment.children[1]; // details
		byline = details.children[0];
		to_collapse.push(main_comment.children[0]); // voters
		to_collapse.push(details.children[1]); // text
	}

	// Only collapse if there are child comments
	if (child_comments.childElementCount > 0) {
		to_collapse.push(child_comments);
	}

	// Add click event hiding the ol of comments
	collapser.addEventListener('click', collapse_expand(to_collapse));

	// Add element to end of byline
	byline.appendChild(collapser);

};

// Retrieve all comment lists (comment and children pairs)
var html_comments = document.querySelectorAll('ol.comments>li');
// Convert NodeList to array to use forEach
var comment_list = Array.prototype.slice.call(html_comments);
// If on story, ignore first 'comment' (it's the comment creation box)
if (document.URL.indexOf("/s/") > -1) {
	comment_list = comment_list.slice(1);
}

comment_list.forEach(add_collapser);